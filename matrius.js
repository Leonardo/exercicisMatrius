let rows = document.querySelector("tbody").children
let matrix = []
for (let i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
function paintAll() {
    erase();
    for ( let i = 0; i < matrix.length; i++ ) { // afegir codi
        for (let j= 0; j < matrix[0].length; j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j= 0; j < matrix[0].length; j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();
    for (let i = 0; i < matrix.length ; i++) { // afegir codi
        for (let j= 0; j < matrix[0].length; j++) { // let j < matrix[i].length/2; j<matrix[i].length; j++  
            if(j > 2){
                matrix[i][j].style.backgroundColor= "red";
            }
        }
    }
}

function paintLeftHalf() {
    erase();

    for (let i = 0; i < matrix.length; i++) { // afegir codi
        for (let j= 0; j < matrix[0].length/2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {
    erase();

    for (let i = 0; i < matrix.length/2; i++) { // 
        for (let j= 0; j < matrix[0].length; j++ ) { //  
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();

    for (let i = 0; i < matrix.length; i++  ) { // 
        for (let j= 0; j<i; j++ ) { // let j = 0; j<i; j++
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperTriangle() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // afegir codi
        for (let j= 0; j<=i; j++){     // if(i==0 || i == (matrix.length-1) || j == 0 ||j== (matrix[i].length-1))){
                                       // matrix[i][j].style.backgroundColor = "red";  
                                       //{
            matrix[j][i].style.backgroundColor = "red";
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) {
        for (let j= 0; j < matrix[0].length; j++) { 
          if(i==0 || i == (matrix.length-1) || j == 0 ||j== (matrix[i].length-1)){
            matrix[i][j].style.backgroundColor = "red";  
          }
          }      
    }
}    


function paintCheckerboard() {
    erase();
    for (let i=0; i < matrix.length; i++ ) { // afegir codi
        for (let j=0;j < matrix[0].length; j++ ) { // afegir codi
           if(i%2==j%2){
            matrix[i][j].style.backgroundColor = "red"
           }
        }
    }
}
function paintCheckerboard2() {
    erase();
    for (let i=0; i < matrix.length; i++ ) {             
        for (let j=0;j < matrix[0].length; j++ ) {      
            if(i%2!==j%2){                                                   
                matrix[i][j].style.backgroundColor = "red";
        
            }       
        }
    } 
}
function paintNeighbours() {
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    
    for (let i = -1; i <= 1; i++) {
        for (let j = -1; j <= 1; j++) {
            if(!(i == 0 && j == 0)){
            try{
                matrix[inputX+i][inputY+j].style.backgroundColor = "red";
            }catch{
                continue;
            }
            }
	    }
    }
}

function countNeighbours(x,y) {
    let count = 0;
    for (let i = -1; i<= 1; i++) {
        for (let j = -1; j<= 1; j++) {
            let suma1 = x+i;
            let suma2 = y+j;
            if(suma1<matrix.length && suma1>0 && suma2<matrix[suma1].length && suma2>0){
                if(matrix[suma1][suma2].style.backgroundColor=="red"){
                    if(suma1!=x || suma2!=y){
                        count++;
                    }
                }
            }
	    }
    }
    return count;
}

function paintAllNeighbours() {
    erase();
    paintRandomly();
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            let count = countNeighbours(i,j);
	        matrix[i][j].innerText = count;
	    }
    }
}
